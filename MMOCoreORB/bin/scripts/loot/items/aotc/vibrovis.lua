
vibrovis = {
	minimumLevel = 0,
	maximumLevel = -1,
	customObjectName = "",
	directObjectTemplate = "object/tangible/component/weapon/vibrovis.iff",
	craftingValues = {
		{"mindamage",0,0,0},
		{"maxdamage",0,0,0},
		{"attackspeed",0,0},
		{"woundchance",0,0,0},
		{"hitpoints",0,0,0},
		{"zerorangemod",0,0,0},
		{"maxrangemod",0,0,0},
		{"midrangemod",0,0,0},
		{"attackhealthcost",0,0,0},
		{"attackactioncost",0,0,0},
		{"attackmindcost",0,0,0},
		{"useCount",1,5,0},
	},
	customizationStringNames = {},
	customizationValues = {}
}

addLootItemTemplate("vibrovis", vibrovis)
